const {Pool} = require('pg')

const pool = new Pool ({
    user:'postgres',
    host:'localhost',
    database:'testDB',
    password:'postgres',
    port: 5432
})
pool.connect();
pool.query('select * from people',(err,result)=>
    {
        console.log(err,result)

    })
const getdata=(req,res)=>
{
console.log("getdata");
    pool.query('select * from People',(err,result)=>
    {
        if(err){
            throw err
        }
        res.status(200).json(result.rows)
    })
}
const insertdata =(req,res)=> {
console.log("inserting");  
    const {Firstname,lastname}=req.body

    pool.query('insert into people(Firstname,lastname) values($1,$2)',
    [Firstname,lastname],
    (err,result)=>{
        if(err){
            console.log(err)
        }
        res.status(200).send('value added successfully')
    })
}
 
module.exports = {getdata,insertdata};

